package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RottenTransformationTest {
    private Class<?> rottenClass;

    @BeforeEach
    public void setup() throws Exception {
        rottenClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.RottenTransformation");
    }

    @Test
    public void testRottenHasEncodeMethod() throws Exception {
        Method translate = rottenClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testRottenEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Tbgjsb boe J xfou up b cmbdltnjui up gpshf pvs txpse";

        Spell result = new RottenTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testRottenHasDecodeMethod() throws Exception {
        Method translate = rottenClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testRottenDecodesCorrectly() throws Exception {
        String text = "Tbgjsb boe J xfou up b cmbdltnjui up gpshf pvs txpse";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new RottenTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }
}
