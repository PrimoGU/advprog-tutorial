package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    @Autowired
    private WeaponRepository weaponRepository;


    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        List<Weapon> weaponList = new LinkedList<Weapon>();

        // Add all weapons
        for (Weapon weapon : weaponRepository.findAll()) {
            weaponList.add(weapon);
        }

        // Add all bows
        for (Bow bow : bowRepository.findAll()) {
            if (weaponRepository.findByAlias(bow.getName()) == null) {
                weaponList.add(new BowAdapter(bow));
            }
        }

        // Add all spellbooks
        for (Spellbook spellbook : spellbookRepository.findAll()) {
            if (weaponRepository.findByAlias(spellbook.getName()) == null) {
                weaponList.add(new SpellbookAdapter(spellbook));
            }
        }
        return weaponList;
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        Bow bow = bowRepository.findByAlias(weaponName);
        Spellbook spellbook = spellbookRepository.findByAlias(weaponName);

        if (weapon == null) {
            weapon = ((bow != null) ? new BowAdapter(bow) : new SpellbookAdapter(spellbook));
        }

        String attackLog;

        if (attackType == 0) {
            attackLog = weapon.normalAttack();
        } else {
            attackLog = weapon.chargedAttack();
        }

        attackLog = weapon.getHolderName() + ": " + attackLog;
        logRepository.addLog(attackLog);
        weaponRepository.save(weapon);
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
