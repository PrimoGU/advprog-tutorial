package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.*;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    private List<Spell> spellList;

    public ChainSpell(List<Spell> spells) {
        this.spellList = spells;
    }

    // Cast setiap Spell pada List satu per satu
    @Override
    public void cast() {
        for (Spell spell : this.spellList) {
            spell.cast();
        }
    }

    // Undo setiap Spell pada List dengan urutan terbalik
    @Override
    public void undo() {
        for (int i = this.spellList.size() - 1; i >= 0; i--) {
            this.spellList.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
