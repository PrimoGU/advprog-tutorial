package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        //ToDo: Complete Me
        this.guild = guild;
    }

    //ToDo: Complete Me

    @Override
    public void update() {
        String tipe = this.guild.getQuestType();

        if (tipe.equals("D") || tipe.equals("E")) {
            this.addQuest(this.guild.getQuest());
        }
    }
}
