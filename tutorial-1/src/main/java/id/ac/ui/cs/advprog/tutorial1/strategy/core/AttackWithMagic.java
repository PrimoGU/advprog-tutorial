package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    //ToDo: Complete me

    @Override
    public String attack() {
        return "Celestial Shower";
    }

    @Override
    public String getType() {
        return "AttackMagic";
    }
}
