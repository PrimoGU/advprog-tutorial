package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {


    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        //ToDo: Complete Me
        this.guild = guild;
    }

    //ToDo: Complete Me

    @Override
    public void update() {
        String tipe = this.guild.getQuestType();

        if (tipe.equals("D") || tipe.equals("R") || tipe.equals("E")) {
            this.addQuest(this.guild.getQuest());
        }
    }
}
