# Lazy vs Eager Instantiation

## Lazy Instantiation
```Lazy Instantiation``` artinya pembuatan objek singleton tersebut dilakukan saat dibutuhkan saja, misalnya pada pertama kali method `getInstance()` dipanggil. Keuntungannya adalah kita bisa menghemat resource apabila pembuatan objek singleton tersebut membutuhkan banyak sumber daya. Kekurangannya adalah agak sulit menangani kasus apabila ada 2 thread yang memanggil `getInstance()` ketika objek singletonnya belum dibuat.

## Eager Instantiation
```Eager Instantiation``` artinya objek singleton dibuat sejak program mulai berjalan. Kelebihannya adalah kita tidak perlu menangani kasus multithreading seperti pada lazy instantiation. Kekurangannya adalah sumber daya dengan jumlah besar terpakai dari awal apabila objek singletonnya berat.
