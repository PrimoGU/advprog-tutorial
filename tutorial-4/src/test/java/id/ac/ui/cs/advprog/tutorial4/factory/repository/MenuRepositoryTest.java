package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MenuRepositoryTest {
    private MenuRepository menuRepository;

    @BeforeEach
    public void setup() {
        menuRepository = new MenuRepository();
        menuRepository.add(new LiyuanSoba("LS"));
        menuRepository.add(new MondoUdon("MU"));
        menuRepository.add(new SnevnezhaShirataki("SS"));
        menuRepository.add(new InuzumaRamen("IR"));
    }

    @Test
    public void testGetMenusShouldReturnCorrectList() {
        assertEquals(menuRepository.getMenus().size(), 4);
    }

    @Test
    public void testAddMenuShouldAddToList() {
        int sizeBefore = menuRepository.getMenus().size();
        menuRepository.add(new LiyuanSoba("LS2"));

        assertEquals(sizeBefore + 1, menuRepository.getMenus().size());
    }

}
