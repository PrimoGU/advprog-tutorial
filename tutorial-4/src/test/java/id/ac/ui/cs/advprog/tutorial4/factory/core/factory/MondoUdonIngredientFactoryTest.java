package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class MondoUdonIngredientFactoryTest {
    private Class<?> mondoUdonIngredientFactoryClass;

    private MondoUdonIngredientFactory mondoUdonIngredientFactory;

    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonIngredientFactoryClass = Class
                .forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MondoUdonIngredientFactory");
        mondoUdonIngredientFactory = new MondoUdonIngredientFactory();
    }

    @Test
    public void testMondoUdonIngredientFactoryClassIsConcreteClass() {
        int classModifiers = mondoUdonIngredientFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testMondoUdonIngredientFactoryIsAnIngredientFactory() {
        Collection<Type> interfaces = Arrays.asList(mondoUdonIngredientFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientFactory")));
    }

    @Test
    public void testMondoUdonIngredientFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = mondoUdonIngredientFactoryClass.getDeclaredMethod("createFlavor");
        int methodModifiers = createFlavor.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMondoUdonIngredientFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = mondoUdonIngredientFactoryClass.getDeclaredMethod("createMeat");
        int methodModifiers = createMeat.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMondoUdonIngredientFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = mondoUdonIngredientFactoryClass.getDeclaredMethod("createNoodle");
        int methodModifiers = createNoodle.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMondoUdonIngredientFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = mondoUdonIngredientFactoryClass.getDeclaredMethod("createTopping");
        int methodModifiers = createTopping.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMondoUdonIngredientFactoryCreateFlavorShouldReturnSalty() {
        Flavor flavor = mondoUdonIngredientFactory.createFlavor();
        assertThat(flavor).isInstanceOf(Salty.class);
    }

    @Test
    public void testMondoUdonIngredientFactoryCreateMeatShouldReturnChicken() {
        Meat meat = mondoUdonIngredientFactory.createMeat();
        assertThat(meat).isInstanceOf(Chicken.class);
    }

    @Test
    public void testMondoUdonIngredientFactoryCreateNoodleShouldReturnUdon() {
        Noodle noodle = mondoUdonIngredientFactory.createNoodle();
        assertThat(noodle).isInstanceOf(Udon.class);
    }

    @Test public void testMondoUdonIngredientFactoryCreateToppingShouldReturnCheese() {
        Topping topping = mondoUdonIngredientFactory.createTopping();
        assertThat(topping).isInstanceOf(Cheese.class);
    }

}
