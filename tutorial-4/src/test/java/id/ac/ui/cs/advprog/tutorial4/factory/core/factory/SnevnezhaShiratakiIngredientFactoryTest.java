package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class SnevnezhaShiratakiIngredientFactoryTest {
    private Class<?> snevnezhaShiratakiIngredientFactoryClass;

    private SnevnezhaShiratakiIngredientFactory snevnezhaShiratakiIngredientFactory;

    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShiratakiIngredientFactoryClass = Class
                .forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.SnevnezhaShiratakiIngredientFactory");
        snevnezhaShiratakiIngredientFactory = new SnevnezhaShiratakiIngredientFactory();
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryClassIsConcreteClass() {
        int classModifiers = snevnezhaShiratakiIngredientFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryIsAnIngredientFactory() {
        Collection<Type> interfaces = Arrays.asList(snevnezhaShiratakiIngredientFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientFactory")));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = snevnezhaShiratakiIngredientFactoryClass.getDeclaredMethod("createFlavor");
        int methodModifiers = createFlavor.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = snevnezhaShiratakiIngredientFactoryClass.getDeclaredMethod("createMeat");
        int methodModifiers = createMeat.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = snevnezhaShiratakiIngredientFactoryClass.getDeclaredMethod("createNoodle");
        int methodModifiers = createNoodle.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = snevnezhaShiratakiIngredientFactoryClass.getDeclaredMethod("createTopping");
        int methodModifiers = createTopping.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryCreateFlavorShouldReturnUmami() {
        Flavor flavor = snevnezhaShiratakiIngredientFactory.createFlavor();
        assertThat(flavor).isInstanceOf(Umami.class);
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryCreateMeatShouldReturnFish() {
        Meat meat = snevnezhaShiratakiIngredientFactory.createMeat();
        assertThat(meat).isInstanceOf(Fish.class);
    }

    @Test
    public void testSnevnezhaShiratakiIngredientFactoryCreateNoodleShouldReturnShirataki() {
        Noodle noodle = snevnezhaShiratakiIngredientFactory.createNoodle();
        assertThat(noodle).isInstanceOf(Shirataki.class);
    }

    @Test public void testSnevnezhaShiratakiIngredientFactoryCreateToppingShouldReturnFlower() {
        Topping topping = snevnezhaShiratakiIngredientFactory.createTopping();
        assertThat(topping).isInstanceOf(Flower.class);
    }

}
