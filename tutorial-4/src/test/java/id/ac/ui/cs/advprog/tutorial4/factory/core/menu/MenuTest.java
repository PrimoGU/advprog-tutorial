package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.mockito.Mock;
import static org.mockito.Mockito.*;

public class MenuTest {
    private Class<?> menuClass;

    private Menu menu;

    @Mock
    private IngredientFactory factory;

    @BeforeEach
    public void setup() throws Exception {
        menuClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
        factory = mock(IngredientFactory.class);
        menu = new Menu("Dummy", factory);
    }

    @Test
    public void testMenuIsAPublicClass() {
        int classModifiers = menuClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testMenuHasGetNameMethod() throws Exception {
        Method getName = menuClass.getDeclaredMethod("getName");
        int methodModifiers = getName.getModifiers();

        assertEquals("java.lang.String", getName.getGenericReturnType().getTypeName());
        assertEquals(0, getName.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMenuHasGetNoodleMethod() throws Exception {
        Method getNoodle = menuClass.getDeclaredMethod("getNoodle");
        int methodModifiers = getNoodle.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertFalse(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getNoodle.getParameterCount());
    }

    @Test
    public void testMenuHasGetMeatMethod() throws Exception {
        Method getMeat = menuClass.getDeclaredMethod("getMeat");
        int methodModifiers = getMeat.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertFalse(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getMeat.getParameterCount());
    }

    @Test
    public void testMenuHasGetToppingMethod() throws Exception {
        Method getTopping = menuClass.getDeclaredMethod("getTopping");
        int methodModifiers = getTopping.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertFalse(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getTopping.getParameterCount());
    }

    @Test
    public void testMenuHasGetFlavorMethod() throws Exception {
        Method getFlavor = menuClass.getDeclaredMethod("getFlavor");
        int methodModifiers = getFlavor.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertFalse(Modifier.isAbstract(methodModifiers));
        assertEquals(0, getFlavor.getParameterCount());
    }

    @Test
    public void testMenuHasCookMethod() throws Exception {
        Method cook = menuClass.getDeclaredMethod("cook");
        int methodModifiers = cook.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals(0, cook.getParameterCount());
    }

    @Test
    public void testMenuCookMethodImplementedCorrectly() throws Exception {
        Noodle noodle = new Ramen();
        Flavor flavor = new Spicy();
        Meat meat = new Pork();
        Topping topping = new BoiledEgg();
        when(factory.createNoodle()).thenReturn(noodle);
        when(factory.createFlavor()).thenReturn(flavor);
        when(factory.createMeat()).thenReturn(meat);
        when(factory.createTopping()).thenReturn(topping);

        menu.cook();

        assertThat(menu.getNoodle()).isInstanceOf(Ramen.class);
        assertThat(menu.getFlavor()).isInstanceOf(Spicy.class);
        assertThat(menu.getMeat()).isInstanceOf(Pork.class);
        assertThat(menu.getTopping()).isInstanceOf(BoiledEgg.class);
    }

    @Test
    public void testMenuGetNameMethodImplementedCorrectly() {
        assertEquals("Dummy", menu.getName());
    }

}
