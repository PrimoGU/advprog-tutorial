package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class InuzumaRamenIngredientFactoryTest {
    private Class<?> inuzumaRamenIngredientFactoryClass;

    private InuzumaRamenIngredientFactory inuzumaRamenIngredientFactory;

    @BeforeEach
    public void setUp() throws Exception {
        inuzumaRamenIngredientFactoryClass = Class
                .forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.InuzumaRamenIngredientFactory");
        inuzumaRamenIngredientFactory = new InuzumaRamenIngredientFactory();
    }

    @Test
    public void testInuzumaRamenIngredientFactoryClassIsConcreteClass() {
        int classModifiers = inuzumaRamenIngredientFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testInuzumaRamenIngredientFactoryIsAnIngredientFactory() {
        Collection<Type> interfaces = Arrays.asList(inuzumaRamenIngredientFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientFactory")));
    }

    @Test
    public void testInuzumaRamenIngredientFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = inuzumaRamenIngredientFactoryClass.getDeclaredMethod("createFlavor");
        int methodModifiers = createFlavor.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testInuzumaRamenIngredientFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = inuzumaRamenIngredientFactoryClass.getDeclaredMethod("createMeat");
        int methodModifiers = createMeat.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testInuzumaRamenIngredientFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = inuzumaRamenIngredientFactoryClass.getDeclaredMethod("createNoodle");
        int methodModifiers = createNoodle.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testInuzumaRamenIngredientFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = inuzumaRamenIngredientFactoryClass.getDeclaredMethod("createTopping");
        int methodModifiers = createTopping.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testInuzumaRamenIngredientFactoryCreateFlavorShouldReturnSpicy() {
        Flavor flavor = inuzumaRamenIngredientFactory.createFlavor();
        assertThat(flavor).isInstanceOf(Spicy.class);
    }

    @Test
    public void testInuzumaRamenIngredientFactoryCreateMeatShouldReturnPork() {
        Meat meat = inuzumaRamenIngredientFactory.createMeat();
        assertThat(meat).isInstanceOf(Pork.class);
    }

    @Test
    public void testInuzumaRamenIngredientFactoryCreateNoodleShouldReturnRamen() {
        Noodle noodle = inuzumaRamenIngredientFactory.createNoodle();
        assertThat(noodle).isInstanceOf(Ramen.class);
    }

    @Test public void testInuzumaRamenIngredientFactoryCreateToppingShouldReturnBoiledEgg() {
        Topping topping = inuzumaRamenIngredientFactory.createTopping();
        assertThat(topping).isInstanceOf(BoiledEgg.class);
    }

}
