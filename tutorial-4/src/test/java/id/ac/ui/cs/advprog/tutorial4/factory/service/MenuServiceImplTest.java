package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MenuServiceImplTest {
    private MenuServiceImpl menuService;

    @BeforeEach
    public void setUp() throws Exception {
        menuService = new MenuServiceImpl();
    }

    @Test
    public void testMenuServiceImplHasGetMenusImplementation() {
        assertEquals(4, menuService.getMenus().size());
    }

    @Test
    public void testMenuServiceImplHasCreateMenuImplementation() {
        menuService.createMenu("Dummy Soba", "LiyuanSoba");
        menuService.createMenu("Dummy Udon", "MondoUdon");
        menuService.createMenu("Dummy Ramen", "InuzumaRamen");
        menuService.createMenu("Dummy Shirataki", "SnevnezhaShirataki");

        assertEquals(8, menuService.getMenus().size());
    }

    @Test
    public void testCreateMenuLiyuanSoba() {
        String menuName = "LS";
        Menu liyuanSoba = menuService.createMenu(menuName, "LiyuanSoba");
        assertTrue(liyuanSoba instanceof  LiyuanSoba);
        assertEquals(liyuanSoba.getName(), menuName);
    }

    @Test
    public void testCreateMenuInuzumaRamen() {
        String menuName = "IR";
        Menu inuzumaRamen = menuService.createMenu(menuName, "InuzumaRamen");
        assertTrue(inuzumaRamen instanceof InuzumaRamen);
        assertEquals(inuzumaRamen.getName(), menuName);
    }

    @Test
    public void testCreateMenuMondoUdon() {
        String menuName = "MU";
        Menu liyuanSoba = menuService.createMenu(menuName, "MondoUdon");
        assertTrue(liyuanSoba instanceof MondoUdon);
        assertEquals(liyuanSoba.getName(), menuName);
    }

    @Test
    public void testCreateMenuSnevnezhaShirataki() {
        String menuName = "SS";
        Menu liyuanSoba = menuService.createMenu(menuName, "SnevnezhaShirataki");
        assertTrue(liyuanSoba instanceof SnevnezhaShirataki);
        assertEquals(liyuanSoba.getName(), menuName);
    }

}
