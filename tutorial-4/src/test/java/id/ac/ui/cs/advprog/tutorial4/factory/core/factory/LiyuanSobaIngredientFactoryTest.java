package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

public class LiyuanSobaIngredientFactoryTest {
    private Class<?> liyuanSobaIngredientFactoryClass;

    private LiyuanSobaIngredientFactory liyuanSobaIngredientFactory;

    @BeforeEach
    public void setUp() throws Exception {
        liyuanSobaIngredientFactoryClass = Class
                .forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.LiyuanSobaIngredientFactory");
        liyuanSobaIngredientFactory = new LiyuanSobaIngredientFactory();
    }

    @Test
    public void testLiyuanSobaIngredientFactoryClassIsConcreteClass() {
        int classModifiers = liyuanSobaIngredientFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertFalse(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testLiyuanSobaIngredientFactoryIsAnIngredientFactory() {
        Collection<Type> interfaces = Arrays.asList(liyuanSobaIngredientFactoryClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.IngredientFactory")));
    }

    @Test
    public void testLiyuanSobaIngredientFactoryOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = liyuanSobaIngredientFactoryClass.getDeclaredMethod("createFlavor");
        int methodModifiers = createFlavor.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0, createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testLiyuanSobaIngredientFactoryOverrideCreateMeatMethod() throws Exception {
        Method createMeat = liyuanSobaIngredientFactoryClass.getDeclaredMethod("createMeat");
        int methodModifiers = createMeat.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0, createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testLiyuanSobaIngredientFactoryOverrideCreateNoodleMethod() throws Exception {
        Method createNoodle = liyuanSobaIngredientFactoryClass.getDeclaredMethod("createNoodle");
        int methodModifiers = createNoodle.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0, createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testLiyuanSobaIngredientFactoryOverrideCreateToppingMethod() throws Exception {
        Method createTopping = liyuanSobaIngredientFactoryClass.getDeclaredMethod("createTopping");
        int methodModifiers = createTopping.getModifiers();

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0, createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testLiyuanSobaIngredientFactoryCreateFlavorShouldReturnSweet() {
        Flavor flavor = liyuanSobaIngredientFactory.createFlavor();
        assertThat(flavor).isInstanceOf(Sweet.class);
    }

    @Test
    public void testLiyuanSobaIngredientFactoryCreateMeatShouldReturnBeef() {
        Meat meat = liyuanSobaIngredientFactory.createMeat();
        assertThat(meat).isInstanceOf(Beef.class);
    }

    @Test
    public void testLiyuanSobaIngredientFactoryCreateNoodleShouldReturnSoba() {
        Noodle noodle = liyuanSobaIngredientFactory.createNoodle();
        assertThat(noodle).isInstanceOf(Soba.class);
    }

    @Test public void testLiyuanSobaIngredientFactoryCreateToppingShouldReturnMushroom() {
        Topping topping = liyuanSobaIngredientFactory.createTopping();
        assertThat(topping).isInstanceOf(Mushroom.class);
    }

}
