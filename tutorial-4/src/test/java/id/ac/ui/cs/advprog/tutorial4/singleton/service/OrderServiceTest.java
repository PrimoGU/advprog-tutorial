package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.mockito.Mock;
import org.mockito.Spy;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class OrderServiceTest {
    private Class<?> orderServiceClass;

    @Mock
    OrderDrink orderDrink;

    @Mock
    OrderFood orderFood;

    @Spy
    OrderServiceImpl orderService = new OrderServiceImpl();

    @BeforeEach
    public void setUp() throws Exception {
        orderServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderService");
        orderDrink = mock(OrderDrink.class);
        orderFood = mock(OrderFood.class);
    }

    @Test
    public void testOrderServiceOrderADrinkImplementedCorrectly() throws Exception {
        String drink = "Dummy Drink";
        doNothing().when(orderDrink).setDrink(isA(String.class));
        when(orderDrink.getDrink()).thenReturn(drink);

        orderService.orderADrink("Dummy Drink");
        assertEquals("Dummy Drink", orderService.getDrink().getDrink());
    }

    @Test
    public void testOrderServiceOrderAFoodImplementedCorrectly() throws Exception {
        String food = "Dummy Food";
        doNothing().when(orderFood).setFood(isA(String.class));
        when(orderFood.getFood()).thenReturn(food);

        orderService.orderAFood("Dummy Food");
        assertEquals("Dummy Food", orderService.getFood().getFood());
    }

    @Test
    public void testOrderADrinkShouldUpdateDrink() {
        orderService.orderADrink("Dummy Drink");
        assertEquals("Dummy Drink", OrderDrink.getInstance().getDrink());
    }

    @Test
    public void testOrderAFoodShouldUpdateFood() {
        orderService.orderAFood("Dummy Food");
        assertEquals("Dummy Food", OrderFood.getInstance().getFood());
    }

}
