# Requirements

### Mahasiswa
- Mahasiswa bisa mendaftar ke tepat 1 mata kuliah
- Pendaftaran asdos langsung diterima
- Mahasiswa yang sudah jadi asdos tidak bisa mendaftar lagi, dan tidak bisa dihapus
- Mahasiswa bisa diupdate data dirinya
- Mahasiswa (asdos) many to one ke Mata Kuliah

### Mata Kuliah
- Mata kuliah bisa memiliki beberapa asdos
- Mata kuliah dengan asdos tidak boleh dihapus
- Mata kuliah bisa diupdate data mata kuliahnya
- Mata Kuliah one to many ke Mahasiswa (asdos)

### Log
- Mahasiswa yang menjadi asdos bisa membuat log
- Log bisa diupdate dan dihapus
- Ada fitur log summary yang merekap honor bulanan jam
- Log many to one ke Mahasiswa (asdos)
  dalam satuan 