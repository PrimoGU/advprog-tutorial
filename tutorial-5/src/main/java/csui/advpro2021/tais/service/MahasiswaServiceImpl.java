package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MahasiswaServiceImpl implements MahasiswaService {
    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @Override
    public Mahasiswa createMahasiswa(Mahasiswa mahasiswa) {
        if (mahasiswaRepository.findByNpm(mahasiswa.getNpm()) == null) {
            mahasiswaRepository.save(mahasiswa);
            return mahasiswa;
        }
        return null;
    }

    @Override
    public Iterable<Mahasiswa> getListMahasiswa() {
        return mahasiswaRepository.findAll();
    }

    @Override
    public Mahasiswa getMahasiswaByNPM(String npm) {
        return mahasiswaRepository.findByNpm(npm);
    }

    @Override
    public Mahasiswa updateMahasiswa(String npm, Mahasiswa mahasiswa) {
        Mahasiswa existingMahasiswa = mahasiswaRepository.findByNpm(npm);
        if (existingMahasiswa != null) {
            mahasiswa.setNpm(npm);
            mahasiswa.setMatkulAsisten(existingMahasiswa.getMatkulAsisten());
            mahasiswaRepository.save(mahasiswa);
            return mahasiswa;
        }
        return null;
    }

    @Override
    public void deleteMahasiswaByNPM(String npm) {
        // Check if actually exists and only delete if is not a TA
        if (mahasiswaRepository.findByNpm(npm) != null &&
                mahasiswaRepository.findByNpm(npm).getMatkulAsisten() == null) {
            mahasiswaRepository.deleteById(npm);
        }
    }

    @Override
    public Mahasiswa registerMatkulAsisten(String npm, String kodeMatkul) {
        Mahasiswa mahasiswa = mahasiswaRepository.findByNpm(npm);
        MataKuliah mataKuliah = mataKuliahRepository.findByKodeMatkul(kodeMatkul);

        // Only add if mahasiswa and matkul is valid
        // and mahasiswa is not currently a TA already
        if (mataKuliah != null && mahasiswa != null && mahasiswa.getMatkulAsisten() == null) {
            mahasiswa.setMatkulAsisten(mataKuliah);
            mataKuliah.getTimAsisten().add(mahasiswa);

            // Persist to DB
            mahasiswaRepository.save(mahasiswa);
            mataKuliahRepository.save(mataKuliah);
        }

        return mahasiswa;
    }
}
