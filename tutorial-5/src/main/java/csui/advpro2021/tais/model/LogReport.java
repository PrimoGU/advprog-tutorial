package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.*;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.*;
import java.util.stream.Collectors;
import java.sql.Timestamp;
import java.text.DateFormatSymbols;

@Data
@Setter
public class LogReport {
    private String month;
    private int jamKerja;
    private int pembayaran;

    /**
     * Creates log report segmented by months from logs.
     * Assumes month as startDate's month (WIB)
     * @param logList Raw Log instances from DB
     * @return list of LogReport per month
     */
    public static List<LogReport> generateLogReport(List<Log> logList) {
        List<LogReport> logReports = new LinkedList<LogReport>();

        // Iterate every month from January to December
        for (int month = 0; month < 12; month++) {

            int finalMonth = month; // Temp final for lambda expression
            List<Log> logForMonth = logList
                    .stream()
                    .filter(log -> log.getStartDate().getMonth() == finalMonth)
                    .collect(Collectors.toList());

            int hours = 0;
            for (Log log : logForMonth) {
                // Convert to milisecs then to hours for accurate conversion (rounded down)
                hours += (log.getEndDate().getTime() - log.getStartDate().getTime()) / (60 * 60 * 1000);
            }

            // Create new logReport instance
            if (hours != 0) {
                LogReport logReport = new LogReport();
                logReport.setJamKerja(hours);
                logReport.setPembayaran(350 * hours);
                logReport.setMonth(new DateFormatSymbols().getMonths()[month]);

                logReports.add(logReport);
            }
        }
        return logReports;
    }
}
