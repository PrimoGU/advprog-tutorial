package csui.advpro2021.tais.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class LogReportTest {

    List<Log> logList;

    @BeforeEach
    public void setUp() {
        Log log1 = new Log(1,
                Timestamp.valueOf("2021-01-26 09:00:00"),
                Timestamp.valueOf("2021-01-26 11:01:15"),
                "Periksa Kuis", null);

        Log log2 = new Log(2,
                Timestamp.valueOf("2021-02-27 15:00:00"),
                Timestamp.valueOf("2021-02-27 18:05:55"),
                "Asistensi", null);

        logList = new LinkedList<Log>();
        logList.add(log1);
        logList.add(log2);
    }

    @Test
    public void testLogReportGenerateLogReport() {
        List<LogReport> logReports = LogReport.generateLogReport(logList);

        Assertions.assertEquals(logReports.size(), 2);
        // Assert february payment and hours
        Assertions.assertEquals(logReports.get(1).getPembayaran(), 3*350);
        Assertions.assertEquals(logReports.get(1).getJamKerja(), 3);

        // Assert january payment and hours
        Assertions.assertEquals(logReports.get(0).getJamKerja(), 2);
        Assertions.assertEquals(logReports.get(0).getPembayaran(), 2*350);
    }
}
