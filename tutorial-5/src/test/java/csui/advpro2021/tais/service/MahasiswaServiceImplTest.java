package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MahasiswaServiceImplTest {
    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @Mock
    private MataKuliahRepository mataKuliahRepository;

    @InjectMocks
    private MahasiswaServiceImpl mahasiswaService;

    private Mahasiswa mahasiswa;

    private MataKuliah matkul;

    private final String NON_EXISTENT_NPM = "1234009999";
    private final String NON_EXISTENT_MK_CODE = "OS512";

    @BeforeEach
    public void setUp(){
        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("1906192052");
        mahasiswa.setNama("Maung Meong");
        mahasiswa.setEmail("maung@cs.ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081317691718");

        matkul = new MataKuliah("GSGS101", "Operating Systems", "IK", new LinkedList<Mahasiswa>());
    }

    @Test
    public void testServiceCreateMahasiswa(){
        mahasiswaService.createMahasiswa(mahasiswa);
        verify(mahasiswaRepository, times(1)).save(mahasiswa);
    }

    @Test
    public void testServiceCreateDuplicateMahasiswaShouldFail() {
        when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);

        mahasiswaService.createMahasiswa(mahasiswa);

        verify(mahasiswaRepository, times(0)).save(mahasiswa);
    }

    @Test
    public void testServiceGetListMahasiswa(){
        Iterable<Mahasiswa> listMahasiswa = mahasiswaRepository.findAll();
        lenient().when(mahasiswaService.getListMahasiswa()).thenReturn(listMahasiswa);
        Iterable<Mahasiswa> listMahasiswaResult = mahasiswaService.getListMahasiswa();
        Assertions.assertIterableEquals(listMahasiswa, listMahasiswaResult);
    }

    @Test
    public void testServiceGetMahasiswaByNpm(){
        lenient().when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(mahasiswa);
        Mahasiswa resultMahasiswa = mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm());
        assertEquals(mahasiswa.getNpm(), resultMahasiswa.getNpm());
    }

    @Test
    public void testServiceDeleteMahasiswa(){
        mahasiswaService.createMahasiswa(mahasiswa);
        when(mahasiswaRepository.findByNpm(any())).thenReturn(mahasiswa);
        mahasiswaService.deleteMahasiswaByNPM("1906192052");
        lenient().when(mahasiswaService.getMahasiswaByNPM("1906192052")).thenReturn(null);
        assertEquals(null, mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm()));
    }

    @Test
    public void testServiceDeleteMahasiswaAsTaShouldFail() {
        mahasiswa.setMatkulAsisten(new MataKuliah());

        mahasiswaService.createMahasiswa(mahasiswa);
        when(mahasiswaRepository.findByNpm(any())).thenReturn(mahasiswa);
        mahasiswaService.deleteMahasiswaByNPM("1906192052");

        verify(mahasiswaRepository, times(0)).delete(mahasiswa);
    }

    @Test
    public void testServiceDeleteMahasiswaNotFoundShouldFail() {
        mahasiswaService.deleteMahasiswaByNPM(NON_EXISTENT_NPM);
        lenient().when(mahasiswaRepository.findByNpm(any())).thenReturn(null);
        verify(mahasiswaRepository, times(0)).delete(mahasiswa);
    }

    @Test
    public void testServiceUpdateMahasiswa() {
        mahasiswaService.createMahasiswa(mahasiswa);
        String currentIpkValue = mahasiswa.getIpk();
        //Change IPK from 4 to 3
        mahasiswa.setIpk("3");

        lenient().when(mahasiswaService.updateMahasiswa(mahasiswa.getNpm(), mahasiswa)).thenReturn(mahasiswa);
        Mahasiswa resultMahasiswa = mahasiswaService.updateMahasiswa(mahasiswa.getNpm(), mahasiswa);

        assertNotEquals(resultMahasiswa.getIpk(), currentIpkValue);
        assertEquals(resultMahasiswa.getNama(), mahasiswa.getNama());
    }

    @Test
    public void testRegisterMatkulAsistenToNonAssistantShouldSucceed() {
        when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(mataKuliahRepository.findByKodeMatkul(matkul.getKodeMatkul())).thenReturn(matkul);

        Mahasiswa result = mahasiswaService
                .registerMatkulAsisten(mahasiswa.getNpm(), matkul.getKodeMatkul());

        assertEquals(result.getMatkulAsisten().getKodeMatkul(), matkul.getKodeMatkul());
        assertEquals(mahasiswa.getNpm(), result.getNpm());
    }

    @Test
    public void testRegisterMatkulAsistenMahasiswaNotFoundShouldFail() {

        lenient().when(mahasiswaRepository.findByNpm(NON_EXISTENT_NPM)).thenReturn(null);
        when(mataKuliahRepository.findByKodeMatkul(matkul.getKodeMatkul())).thenReturn(matkul);

        Mahasiswa result = mahasiswaService
                .registerMatkulAsisten(NON_EXISTENT_NPM, matkul.getKodeMatkul());

        verify(mataKuliahRepository, times(0)).save(matkul);
        assertEquals(null, result);
    }

    @Test
    public void testRegisterMatkulAsistenMatkulNotFoundShouldFail() {
        when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        lenient().when(mataKuliahRepository.findByKodeMatkul(NON_EXISTENT_MK_CODE)).thenReturn(null);

        Mahasiswa result = mahasiswaService
                .registerMatkulAsisten(mahasiswa.getNpm(), NON_EXISTENT_MK_CODE);

        verify(mahasiswaRepository, times(0)).save(mahasiswa);
        assertEquals(null, result.getMatkulAsisten());
    }

    @Test
    public void testRegisterMatkulButAlreadyAnAssistantShouldFail() {
        mahasiswa.setMatkulAsisten(matkul);

        when(mahasiswaRepository.findByNpm(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(mataKuliahRepository.findByKodeMatkul(matkul.getKodeMatkul())).thenReturn(matkul);

        mahasiswaService.registerMatkulAsisten(mahasiswa.getNpm(), matkul.getKodeMatkul());

        verify(mahasiswaRepository, times(0)).save(mahasiswa);
        verify(mataKuliahRepository, times(0)).save(matkul);
    }

}
